#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re
import os
from bs4 import BeautifulSoup
import dryscrape

##########################################################################

def get_text(tag):

   text = tag.get_text().encode('utf-8')
   lines = (line.strip() for line in text.splitlines())
   chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
   text = ' '.join(chunk for chunk in chunks if chunk)
   return text

##########################################################################

url = "https://www.washingtonpost.com/graphics/national/police-shootings/"

# Use dryscrape to get all rendered javascript information

sess = dryscrape.Session()
sess.visit(url)
html = sess.body()

# After inspecting the above html, the information about all people is stored
# in a class named 'listWrapper cf'. Get these entries with BeautifulSoup:

soup = BeautifulSoup(html, 'lxml')

entries = soup.find_all('div', {'class': 'listWrapper cf'})

count = 1

for entry in entries:

   # Metadata carries information about the gender:
   meta   =  entry.find(class_ = 'listMetaWrapper')
   gender =  meta.find('p')

   if get_text(gender) != 'Female': continue

   name   =  entry.find(class_ = 'listName')
   blurb  =  entry.find(class_ = 'listBlurb')

   o = str(count) + ") " + get_text(name) + get_text(blurb)  

   print o 

   count += 1


