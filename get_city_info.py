#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re
import os
import urllib2
from bs4 import BeautifulSoup

#################################################################
if (len(sys.argv) != 2):
        print "\nUSAGE ::   <list of cities>\n"
        sys.exit()
#################################################################

'''
set global variables / parameters
'''

# base url to scrape
BASE_URL     =   "https://el.wikipedia.org/wiki/"

# features whose values will be retrieved
REQUEST_INFO =   { 'Πόλη': 1, 'Πληθυσμός': 1, 'Έκταση': 1, 'Ιστοσελίδα': 1, 'Ιστότοπος':1 }

# same as above, but in the requested order
ORDER	     =   [ 'Πόλη', 'Πληθυσμός', 'Έκταση', 'Ιστοσελίδα']

# name of table in html where the above info lies 
TABLE_NAME   =   "infobox geography vcard"

# delimiter for output
DEL          =   ' ||| '

#################################################################
 
def get_html(city):
 
   url = BASE_URL + city
   html = urllib2.urlopen(url).read()
   return html

#################################################################

def get_table(html):

   soup = BeautifulSoup( html, 'lxml' )
   table = soup.find('table', class_=TABLE_NAME)
   return table

#################################################################

def get_row_info(row, store):

   # Each piece of data we are looking for is stored in two columns of some row in the table. 
   # The first column, which may have the header tag (th), holds the attribute and
   # the second column holds the sought value of the attribute.
   
   column = row.findAll(['th', 'td'])
   if len(column) != 2: return
   h = get_text(column[0])
   
   if h not in REQUEST_INFO: return

   if (h == 'Ιστοσελίδα') or (h == 'Ιστότοπος'):
      h = 'Ιστοσελίδα'
      info = row.find('a').get('href')
   else:
      info = get_text(column[1])

   info = re.sub( r'\s?\[\d+\]', '', info ) # remove references
   store[h] = info

#################################################################

def get_text(tag):

   text = tag.get_text().encode('utf-8')
   lines = (line.strip() for line in text.splitlines())
   chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
   text = ' '.join(chunk for chunk in chunks if chunk)
   return text

#################################################################

def write_data(OUT, store):

   output = []
   for h in ORDER:
      if h not in store:
         output.append('N/A')
      else:
         output.append(city_info[h])
   o = DEL.join(output) + '\n'
   OUT.write( o )

#################################################################

OUT = open('info_cities.txt', 'w')
o = DEL.join( ORDER ) + '\n'
OUT.write( o )

with open(sys.argv[1], 'r') as IN:
   for city in IN:
 
      city = city.rstrip()

      # 'city_info' is a hash table and will store
      # the attributes (keys) with their respective sought values

      city_info = {}
      city_info[ORDER[0]] = city
      
      html = get_html(city)
      table = get_table(html)

      if table:
         rows = table.findAll('tr')
         if rows:
            for row in rows:      
               get_row_info(row, city_info)
    
      write_data(OUT, city_info)

OUT.close()

