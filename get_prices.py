#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re
import os
import urllib2
import json
from bs4 import BeautifulSoup
from pprint import pprint

##########################################################################

'''
  Declare global variables:

'''

BASE_URL  =   "https://www.anesishome.gr"

LINK      =   "/βρεφικά-159#!/"

# number of pages:
N_O_P     =   5 

# class name for next page:
NEXT_PAGE =   "pagination_next"

# JS dict with the desired data
VAR_NAME  =   "var analytics_products"

# Souhght attributes of the above dict
(POSIT, NAME, PRICE) =  ("position", "name", "price")

# Delimiter for writing output
DEL       =   " ||| "

##########################################################################

def get_soup_and_next_page(link, page_number):

   if page_number == 1:
      url =  BASE_URL + link
   else:
      url =  BASE_URL + link.encode('utf-8')

   html      =  urllib2.urlopen( url ).read()
   soup      =  BeautifulSoup(html, 'lxml')
   next_page =  soup.find(class_ = NEXT_PAGE)
   link      =  next_page.find('a').get('href')

   return soup, link

##########################################################################

def get_items_prices(soup):

   # Get all javascript code:

   for script in soup.findAll("script"):

      script = script.string
      if script is None: continue
      script = script.encode('utf-8')

      # Search for var_name:

      m = re.search( r'%s = (.*?);' % VAR_NAME, script ) 

      if m is None: continue

      # Found! Now convert the javascript dict into a python dict:

      data = json.loads(m.group(1))

      # Use pprint to inspect the structure of the data:
      # pprint(data)
      # So, 'data' is a dict to a dict to stuff. 

      return data

##########################################################################

def print_output(data, page_number):

   OUT = open('prices.txt', 'a+')

   for item in sorted( data.values(), key=lambda x: int(x[POSIT]) ):
      
      label  = str(page_number) + "." + str(item[POSIT])

      o  = label
      o += DEL
      o += item[NAME].encode('utf-8')
      o += DEL
      o += item[PRICE].encode('utf-8')
      o += "\n"
   
      OUT.write( o )

   OUT.close()

##########################################################################

page_number = 1
link = LINK

while( page_number <= N_O_P ):

   (soup, link) = get_soup_and_next_page( link, page_number )

   data = get_items_prices( soup )

   print_output( data, page_number )

   page_number += 1

##########################################################################        

